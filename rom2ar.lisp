
(defun cli-args ()
  (or 
   #+CLISP *args*
   #+SBCL *posix-argv*  
   #+LISPWORKS system:*line-arguments-list*
   #+CMU extensions:*command-line-words*
   nil))

(defun main ()
  (let ((roman (cadr (cli-args))))
    (print (rom2ar roman))))

(defun num (c)
  (case c
    (#\I 1)
    (#\V 5)
    (#\X 10)
    (#\L 50)
    (#\C 100)
    (#\D 500)
    (#\M 1000)
    (otherwise (error "Character not valid in a roman numeral"))))

(defmethod rom2ar ((roman string))
  (let ((chars (coerce (string-upcase roman) 'list)))
    (when chars
      (rom2ar (mapcar #'num chars)))))

(defmethod rom2ar ((roman list))
  (if (not roman)
    0
    (sum (car roman) (cdr roman))))

(defun sum (head rst)
  (let ((fstn head)
        (snd (car rst))
        (rrst (cdr rst)))
    (cond ((not snd) fstn)
          ((< fstn snd) (+ (- snd fstn) (rom2ar rrst)))
          (t (+ fstn (rom2ar rst))))))

(main)
